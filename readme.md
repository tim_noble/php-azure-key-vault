Step 1

Create an Azure Key Vault

Step 2

a. Create an App Registration
b. Create a new client secret and make a note of it

Step 3

Grant your App access to your Key Vault You do this by:
	a. Go to Access Policies
	b. Click Add Policy
	c. Click Select Principal
	d. Enter the Client ID of the App you registered in 2 above

Step 4

Use the Client Credentials flow to get an access token

Step 5

Make a call to the Secret endpoint and send the access token from Step in an Authorization header. For example "Authorization: Bearer <enter_your_token_here>"