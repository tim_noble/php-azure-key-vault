<?php

function getToken($ini_array) {

	$ch = curl_init();

	try {

		$token_url = $ini_array['AUTH_BASE_URI'] . '/' . $ini_array['TENANT_ID'] . $ini_array['TOKEN_ENDPOINT'];

		$form_data = array(
			'grant_type' => 'client_credentials',
			'client_id' => $ini_array['CLIENT_ID'],
			'client_secret' => $ini_array['CLIENT_SECRET'],
			'resource' => $ini_array['RESOURCE']
		);

		$post_fields = http_build_query($form_data);

		curl_setopt($ch, CURLOPT_URL,$token_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_fields);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		if (!curl_errno($ch)) {

			switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$my_json = json_decode($server_output);
					return $my_json->access_token;
					break;
				default:
					throw Exception("cURL returned a " . $http_code . " status code");
			};

		}
	} catch (Exception $e) {
		throw $e;
	} finally {
		curl_close ($ch);
	}

}

function getSecret($ini_array, $access_token, $secret_name) {

	$ch = curl_init();

	try {
		# build a url
		$secret_url = $ini_array['AKV_BASE_URI'] . $ini_array['AKV_SECRETS_ENDPOINT'] . $secret_name;
		$secret_url .= '?' . http_build_query(array('api-version'=>$ini_array['SECRETS_API_VERSION']));

		curl_setopt($ch, CURLOPT_URL,$secret_url);

		# pass an auth header with the access token
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Bearer ' . $access_token
		));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		# make the call and wait for the response
		$server_output = curl_exec($ch);

		if (!curl_errno($ch)) {
			switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
				case 200:  # OK
					$my_json = json_decode($server_output);
					return $my_json->value;
					break;
				default:
					throw Exception("cURL returned a " . $http_code . " status code");
			}
		}
	} catch (Exception $e) {
		throw $e;
	} finally {
		curl_close ($ch);
	}

}

$ini_array = parse_ini_file("config.ini");

$secret_name = '/SnowflakeAzureStorageKey';

$access_token = getToken($ini_array);

$secret = getSecret($ini_array, $access_token, $secret_name);

print($secret);